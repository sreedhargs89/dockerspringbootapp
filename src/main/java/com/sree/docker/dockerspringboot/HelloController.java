package com.sree.docker.dockerspringboot;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
public class HelloController {

@RequestMapping(value="/docker")
    public String printDocker() throws UnknownHostException {

    InetAddress localhost = InetAddress.getLocalHost();
    System.out.println("System IP Address : " +
            (localhost.getHostAddress()).trim());

    return "Hello from Docker Container and IP Address: "+localhost;
}

}
