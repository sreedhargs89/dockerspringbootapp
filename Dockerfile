FROM openjdk:8
ADD target/dockerspringboot-0.0.1-SNAPSHOT.jar dockerspringboot-0.0.1-SNAPSHOT.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "dockerspringboot-0.0.1-SNAPSHOT.jar"]
