# Docker

### Docker build:

Execute it from the directory where you have DockerFile.

[DockerFile](https://bitbucket.org/sreedhargs89/dockerspringbootapp/src/8b53ff8994bb/Dockerfile?at=master)

    docker build -t springboot/docker .

-t : specifies the image name - Optionally ‘username/imagename:tag’ format.

### Running Container

    docker run -d -p **8085**:8085 --name springboot springboot/docker

-d  Run in detached mode

-p public port 6379 to host machine

-- name container name

last argument - Image name